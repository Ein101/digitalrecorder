<?php
  include_once("class.sqlite.php");
  include_once("class.model.php");

  class Control {
      private $Database;
      private $Template;

      public function __construct() {
        $this->Database = new Database();
        $this->Template = new Model();
      }

      public function CallStructure() {
        $CallRequest = isset($_REQUEST["page"])? $_REQUEST["page"] : "home";
        switch($CallRequest) {
          case 'home':
            $ListRecorder = $this->Template->CallListRecorder();
            include_once('home.php');
          break;

          case 'recording':
            include_once('recording.php');
          break;

          case 'save':
            $InsertRecording = $this->Template->CallInsertRecorder();
          break;

          case 'play':
            $PlayRecorder = $this->Template->CallPlayRecorder($_POST["id"]);
            echo json_encode($PlayRecorder);
          break;

          case 'remove':
            $RemoveRecorder = $this->Template->CallRemoveRecorder($_POST["id"]);
          break;

          default:
            return false;
        }
      }
  }
?>
