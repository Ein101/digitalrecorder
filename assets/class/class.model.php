<?php
  class Model {

    public function CallInsertRecorder() {
      $name_recorder = $_POST['name'];
      $value_recorder = serialize($_POST['keymemory']);

      $database = new Database();
      $sql = "INSERT INTO tb_recorder (name_recorder, value_recorder) VALUES ( '$name_recorder','$value_recorder')";
      $sqlt = $database->query($sql);
      $database->close();
    }

    public function CallListRecorder() {
      $i = 0;
      $row = array();
      $database = new Database();
      $sql = "SELECT
      tb_recorder.id_recorder AS id,
      tb_recorder.name_recorder AS name
      FROM tb_recorder
      ORDER BY id_recorder DESC";

      $sqlt = $database->query($sql);
      while($res = $sqlt->fetchArray(SQLITE3_ASSOC)){
        $row[$i]['id'] = $res['id'];
        $row[$i]['name'] = $res['name'];
        $i++;
      }
      return $row;
      $database->close();
    }

    public function CallPlayRecorder($id) {
      $i = 0;
      $row = array();
      $database = new Database();
      $sql = "SELECT
      tb_recorder.value_recorder AS value
      FROM tb_recorder
      WHERE tb_recorder.id_recorder = '$id'
      LIMIT 1";

      $sqlt = $database->query($sql);
      while($res = $sqlt->fetchArray(SQLITE3_ASSOC)){
        $row = unserialize($res['value']);
        $i++;
      }
      return $row ;
      $database->close();
    }

    public function CallRemoveRecorder($id) {
      $database = new Database();
      $sql = "DELETE FROM tb_recorder WHERE tb_recorder.id_recorder='$id'";
      $sqlt = $database->query($sql);
      $database->close();
    }
  }
?>
