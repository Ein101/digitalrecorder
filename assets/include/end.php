<!-- MODAL -->
<div class="modal fade" role="dialog" tabindex="-1" id="savesession">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Save Session</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="icon ion-android-create"></i>
            </span>
          </div>
          <input id="name_recorder" name="name_recorder" class="form-control" type="text" placeholder="Session Name">
          <div class="input-group-append"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
        <button class="btn btn-success" type="button" onclick="CallSaveMemory()">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- MODAL END -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/custom.js"></script>
  </body>
</html>
