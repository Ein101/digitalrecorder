var keymemory = [];
var keyitems = [];
var keylabel = ['F1','F2','F3','1','2','3','4','5','6','7','8','9','*','0','#'];
var keysound = "beep.mp3";

$( document ).ready(function() {
  CallKeyboard();

  $( 'button[data-type="key"]' ).on( "click", function(e) {
    e.preventDefault();
    id = $(this).attr('id');
    value = $(this).text();
    CallButtonSound();
    CallKeyMemory(id,value);
  });

});

function CallCreateKeyboard( id,name ) {
  return(
    $('<div class="col-4 col-md-4 col-sm-4 p-0"><button id="'+id+'" data-type="key" type="button" class="btn btn-outline-dark btn-lg app-click-set">'+name+'</button></div>')
  );
}

function CallKeyboard() {
  $.each(keylabel, function( id, label) {
    keyitems.push( CallCreateKeyboard(id,label) );
    $( "#keyboard" ).append( keyitems );
    if(id < 3) {
      $('#'+id).removeClass('btn-outline-dark').addClass('btn-info');
    }
  });
}


function CallKeyMemory(id,value) {
    keymemory.push({ id : id, value : value });
}

function CallPlaySession(array) {
  var sq = 5;
  var obj = jQuery.parseJSON(array);
  $.each(obj, function( id, label) {
    sq = sq++ +500;
    $('#'+label['id']).delay(sq).fadeOut(250, function(){
      CallButtonSound();
    }).fadeIn(250);
  });
}

function CallButtonSound() {
  new Audio('assets/media/'+keysound).play();
}

function CallSaveMemory() {
  $.post( "index.php?v="+unique_requestid(), {
    page: 'save',
    keymemory: keymemory,
    name: $('#name_recorder').val()
  },
    function(postData, textStatus, jqXHR) {
      if(textStatus == 'success') {
        window.location.href = 'index.php';
      }
      else {
        console.log('error');
      }
  });
}

function CallPlayMemory(id) {
  $.post( "index.php?v="+unique_requestid(), {
    page: 'play',
    id: id
  },
    function(postData, textStatus, jqXHR) {
      CallPlaySession(postData);
  });
}

function CallRemoveMemory(id) {
  $.post( "index.php?v="+unique_requestid(), {
    page: 'remove',
    id: id
  },
    function(postData, textStatus, jqXHR) {
      if(textStatus == 'success') {
        window.location.href = 'index.php';
      }
      else {
        console.log('error');
      }
  });
}

function unique_requestid() {
  var timestamp = Number(new Date()).toString();
  var random = Math.random() * (Math.random() * 100000 * Math.random() );
  var unique = new String();
  unique = timestamp + random;
  return '?nocache='+unique;
}
