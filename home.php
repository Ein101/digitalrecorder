<?Php include (PATHINCLUDE . 'start.php') ?>
<section id="wrap">
  <div class="container app-wrap-content">
    <div class="row">
      <div class="col-12 col-md-12 col-sm-12">
        <div id="keyboard" class="row"></div>
      </div>
      <div class="col-12 col-md-12 col-sm-12">
        <div class="mt-4">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a class="btn btn-secondary btn-sm" href="index.php?page=recording">
                <i class="icon ion-record"></i>&nbsp;RECORDING
              </a>
            </li>
          </ul>
          <hr>
          <h6 class="text-center text-dark app-set-title">
          <i class="icon ion-cloud"></i>&nbsp;<strong>Saved Sessions</strong></h6>
          <ul class="list-group">
          <? foreach ($ListRecorder as $i => $arr) : ?>
            <li class="list-group-item">
             <span><?=$arr['name']?></span>
              <div class="btn-group float-right" role="group">
                <button class="btn btn-secondary btn-sm" type="button" onclick="CallPlayMemory('<?=$arr['id']?>')"><i class="icon ion-play"></i></button>
                <button class="btn btn-danger btn-sm" type="button" onclick="CallRemoveMemory('<?=$arr['id']?>')" ><i class="icon ion-trash-b"></i></button>
              </div>
            </li>
          <? endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<?Php include (PATHINCLUDE . 'end.php') ?>
