<?Php include (PATHINCLUDE . 'start.php') ?>
<section id="wrap">
  <div class="container app-wrap-content">
    <div class="row">
      <div class="col-12 col-md-12 col-sm-12">
        <div id="keyboard" class="row"></div>
      </div>
      <div class="col-12 col-md-12 col-sm-12">
        <div class="mt-4">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a class="btn btn-danger btn-sm" href="index.php?page=recording">
                <i class="icon ion-record"></i>&nbsp;RECORDING
              </a>
            </li>
            <li class="list-inline-item">
              <button id="save" class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#savesession">
                <i class="icon ion-stop"></i>&nbsp;STOP / SAVE
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<?Php include (PATHINCLUDE . 'end.php') ?>
